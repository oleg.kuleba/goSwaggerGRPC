// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/oleg.kuleba/goSwaggerGRPC/grpcClient/client"
	pb "gitlab.com/oleg.kuleba/goSwaggerGRPC/rpc"
	"gitlab.com/oleg.kuleba/goSwaggerGRPC/validation"
)

// editContactCmd represents the editContact command
var editContactCmd = &cobra.Command{
	Use:   "editContact",
	Short: "editContact phoneNumber name city street building [apartment] - редактирует существующий контакт в файле по номеру телефона",
	Long: `Команда редактирует существующий контакт в файле  по номеру телефона. Т.е. введенный номер должен уже находиться в файле, он останется неизменным.
А остальные аргументы - это новые данные, которые будут записаны вместо старых.
Требует передачу минимум 5-ти, максимум 6-ти аргументов:
номерТелефона имяВладельца город улица дом [квартира](в указанном порядке)
параметры в квадратных скобках являются опциональными
Например: addContact +380501234567 Anya Dnipro Gagarina 103a 5`,
	Args: cobra.MinimumNArgs(5),
	Run:  editContact,
}

func editContact(cmd *cobra.Command, args []string) {
	if !validation.CheckParamsExceptApartment(args[0], args[1], args[2], args[3], args[4]) { // Если аргументы не проходят валидацию (все, кроме квартиры, т.к. она опциональная), выводим инфу об этом и выходим
		validation.PrintValidationMessages()
		return
	}

	// Собираем контакт в структуру
	contact := &pb.Contact{
		Name: args[1],
		Address: &pb.Contact_Address{
			City:     args[2],
			Street:   args[3],
			Building: args[4],
		},
	}

	if len(args) > 5 { // Если квартира указана, то
		if !validation.Validate(args[5], validation.BuildingOrApartmentFlag) { // валидируем ее. Если не валидно - выходим
			validation.PrintValidationMessages()
			return
		}
		contact.Address.Apartment = args[5] // Вписываем в адрес
	}

	// Отдаем в utils на запись
	if flag, msg := client.EditContact(args[0], contact); flag == false {
		fmt.Println("Запись не изменена")
		fmt.Println(msg)
	} else {
		fmt.Println("Запись успешно изменена")
	}
}

func init() {
	rootCmd.AddCommand(editContactCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// editContactCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// editContactCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
