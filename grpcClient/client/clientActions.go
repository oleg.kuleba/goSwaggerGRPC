package client

import (
	"fmt"
	pb "gitlab.com/oleg.kuleba/goSwaggerGRPC/rpc"
	"google.golang.org/grpc"
	"golang.org/x/net/context"
	"io"
	"github.com/golang/protobuf/ptypes/empty"
)

const ServerAddress string = "localhost:50051"

func getConnAndClient(address string, opt grpc.DialOption) (*grpc.ClientConn, pb.AddressBookClient) {
	conn, err := grpc.Dial(ServerAddress, grpc.WithInsecure())
	Check(err)
	client := pb.NewAddressBookClient(conn)
	return conn, client
}

func AddContact(number string, contact *pb.Contact) (bool, string) {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()

	phone := &pb.Phone{
		Number: number,
	}
	contactRequest := &pb.ContactRequestOrResponse{}
	contactRequest.Contact = contact
	contactRequest.Phone = phone

	res, err := client.AddContact(context.Background(), contactRequest)
	Check(err)
	return res.IsOk, res.Msg
}

func FindAll() {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()
	stream, err := client.FindAll(context.Background(), &empty.Empty{})
	Check(err)
	for {
		contactResponse, err := stream.Recv()
		if err == io.EOF {
			break
		}
		Check(err)
		PrintContact(contactResponse.Phone.Number, contactResponse.Contact)
	}
}

func FindByNumber(number string) {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()
	numberRequest := &pb.Phone{
		Number: number,
	}
	contactResponse, err := client.FindByNumber(context.Background(), numberRequest)
	Check(err)

	if contactResponse.Contact != nil {
		contact := contactResponse.Contact
		fmt.Println("Найдена запись:")
		PrintContact(number, contact)
	} else { // Если номера нет в БД - сообщаем об этом
		fmt.Println("Запись с таким номером не существует")
	}
}

func EditContact(number string, contact *pb.Contact) (bool, string) {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()

	phone := &pb.Phone{
		Number: number,
	}
	contactRequest := &pb.ContactRequestOrResponse{}
	contactRequest.Phone = phone
	contactRequest.Contact = contact

	res, err := client.EditContact(context.Background(), contactRequest)
	Check(err)
	return res.IsOk, res.Msg
}

func DeleteContact(number string) (bool, string) {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()

	phone := &pb.Phone{
		Number: number,
	}

	res, err := client.DeleteContact(context.Background(), phone)
	Check(err)
	return res.IsOk, res.Msg
}

func PrintContact(phone string, contact *pb.Contact) {
	fmt.Println("contact {")
	fmt.Println("PHONE:", phone)
	fmt.Println("NAME:", contact.Name)
	fmt.Println("ADDRESS:", contact.Address)
	fmt.Println("}")
}

func Check(e error) {
	if e != nil {
		panic(e)
	}
}
