package validation

import (
	"regexp"
	"fmt"
	"strings"
)

const PhoneFlag string = "phone"
const NameFlag string = "name"
const CityFlag string = "city"
const StreetFlag string = "street"
const BuildingOrApartmentFlag string = "buildingOrApartment"

var PhoneRegexp *regexp.Regexp = regexp.MustCompile("^[+]380([0-9]{9})$")
var NameRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{3,15}$")
var CityRegexp *regexp.Regexp = regexp.MustCompile("^[A-Za-z]{3,15}$")
var StreetRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{4,20}$")
var BuildingOrApartmentRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{1,5}$")

func CheckParamsExceptApartment(phone, name, city, street, building string) bool { // Если аргументы не проходят валидацию (все, кроме квартиры, т.к. она опциональная), выводим инфу об этом
	if Validate(phone, PhoneFlag) && Validate(name, NameFlag) && Validate(city, CityFlag) && Validate(street, StreetFlag) && Validate(building, BuildingOrApartmentFlag) {
		return true
	}
	return false
}

func Validate(data string, flag string) bool {
	switch flag {
	case PhoneFlag:
		return PhoneRegexp.MatchString(data)
	case NameFlag:
		return NameRegexp.MatchString(data)
	case CityFlag:
		return CityRegexp.MatchString(data)
	case StreetFlag:
		return StreetRegexp.MatchString(data)
	case BuildingOrApartmentFlag:
		return BuildingOrApartmentRegexp.MatchString(data)
	default:
		return false
	}
}

var validationMessage = []string{
	"Попробуйте еще раз. При этом введите верные данные",
	"Формат номера телефона +380xxYYYYYYY",
	"Имя - только буквы A-z и/или цифры (от 3 до 15 символов)",
	"Город - только буквы A-z (от 3 до 15 символов)",
	"Улица - только буквы A-z и/или цифры (от 4 до 20 символов)",
	"Дом/квартира - только буквы A-z и/или цифры (от 1 до 5 символов)",
}

func PrintValidationMessages()  {
	for _, val := range validationMessage {
		fmt.Println(val)
	}
	//fmt.Println(strings.Join(validationMessage, "\r\n"))
}

func GetValidationMessages() string {
	return strings.Join(validationMessage, ". ")
}