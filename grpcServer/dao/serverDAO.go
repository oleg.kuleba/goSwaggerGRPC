package dao

import (
	"github.com/boltdb/bolt"
	"path/filepath"
	"log"
	"time"
	pb "gitlab.com/oleg.kuleba/goSwaggerGRPC/rpc"
	"github.com/golang/protobuf/proto"
)

const DBName string = "../grpcServer/phoneBook.db"
const ContactsBucketName string = "contacts"

const MsgContactAlreadyExist string = "Запись с таким номером уже существует. Для изменения данных используйте команду editContact"
const MsgContactNonExist string = "Запись с таким номером не существует"
const MsgEmptyAddressBook string = "Записи в базе данных пока отсутствуют. Можете добавить запись с помощью команды addContact"

type dbWorker func(tx *bolt.Tx) error

// Ф-я подготовки работы БД (получение пути к БД, открытие/закрытие, проверка ошибок)
func dbPrepare(fn dbWorker, isWritableAction bool) error {
	// Получаем путь к БД
	path, err := filepath.Abs(DBName)
	if err != nil {
		log.Fatalf("did not find the path %v. Error: %v", path, err)
	}
	// Открываем БД
	db, err := bolt.Open(path, 0600, &bolt.Options{Timeout: 3 * time.Second})
	if err != nil {
		log.Fatalf("did not open DB %v. Error: %v", DBName, err)
	}
	defer db.Close()

	if isWritableAction {
		if err := db.Update(fn); err != nil { // Открываем Writable транзакцию
			return err
		}
	} else {
		if err := db.View(fn); err != nil { // Или Read-only транзакцию
			return err
		}
	}
	return nil
}

func AddContact(contactRequest *pb.ContactRequestOrResponse) (*pb.BoolMessageReply, error) {
	response := &pb.BoolMessageReply{}
	err := dbPrepare(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(ContactsBucketName))
		if err != nil {
			return err
		}
		if bucket.Get([]byte(contactRequest.Phone.Number)) != nil { // Проверяем наличие номера в БД (для обеспечения уникальности)
			response.IsOk = false
			response.Msg = MsgContactAlreadyExist
		} else { // Если номера в БД нет - добавляем
			if contact, err := proto.Marshal(contactRequest.Contact); err == nil {
				if err = bucket.Put([]byte(contactRequest.Phone.Number), contact); err != nil {
					return err
				}
				response.IsOk = true
			} else {
				return err
			}
		}
		return nil
	}, true) // Открываем Writable транзакцию
	return response, err
}

func FindAll(stream pb.AddressBook_FindAllServer) error {
	err := dbPrepare(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			contactResp := &pb.ContactRequestOrResponse{}
			cursor := bucket.Cursor()
			if k, _ := cursor.First(); k == nil { // Если cursor баккета пустой (ключей/записей нет) - сообщаем об этом и выходим
				//TODO оповещать о пустой БД
				return nil
			}
			// Считываем контакты и отправлем в потоке на клиент
			for k, v := cursor.First(); k != nil; k, v = cursor.Next() {
				contact := &pb.Contact{}
				if err := proto.Unmarshal(v, contact); err != nil {
					return err
				}
				// Записываем номер и контакт в структуру ContactRequestOrResponse{} и отправляем на клиент
				contactResp.Contact = contact
				contactResp.Phone = &pb.Phone{Number: string(k)}
				// TODO добавить статус
				if err := stream.Send(contactResp); err != nil {
					// TODO добавить статус -> status = SERVER-ERROR -> return nil???
					return err
				}
			}
		} else { // Если bucket не существует (совсем нет записей) - сообщаем об этом
			//TODO оповещать о пустой БД
		}
		return nil
	}, false) // Открываем Read-only транзакцию
	return err
}

func FindByNumber(phone *pb.Phone) (*pb.ContactRequestOrResponse, error) {
	contactResp := &pb.ContactRequestOrResponse{}
	err := dbPrepare(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			if contactBytes := bucket.Get([]byte(phone.Number)); contactBytes != nil { // Если номер есть в БД - возвращаем его
				contact := &pb.Contact{}
				err := proto.Unmarshal(contactBytes, contact)
				if err != nil {
					return err
				}
				contactResp.Phone = phone
				contactResp.Contact = contact
				// TODO М/Б добавить статус
			} else { // Если номера в БД нет - сообщаем об этом
				// TODO М/Б оповещать об отсутствии номера
			}
		} else { // Если bucket не существует (совсем нет записей) - сообщаем об этом
			//TODO оповещать о пустой БД
		}
		return nil
	}, false) // Открываем Read-only транзакцию
	return contactResp, err
}

func EditContact(contactRequest *pb.ContactRequestOrResponse) (*pb.BoolMessageReply, error) {
	response := &pb.BoolMessageReply{}
	err := dbPrepare(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			if contactFromDB := bucket.Get([]byte(contactRequest.Phone.Number)); contactFromDB != nil { // Проверяем наличие номера в БД
				contactToDB, err := proto.Marshal(contactRequest.Contact)
				if err != nil {
					return err
				}
				err = bucket.Put([]byte(contactRequest.Phone.Number), contactToDB) // Перезаписываем контакт в БД
				if err != nil {
					return err
				}
				response.IsOk = true
			} else { // Если номера нет в БД - сообщаем об этом
				response.IsOk = false
				response.Msg = MsgContactNonExist
			}
		} else { // Если bucket не существует (совсем нет записей) - сообщаем об этом
			response.IsOk = false
			response.Msg = MsgEmptyAddressBook
		}
		return nil
	}, true) // Открываем Writable транзакцию
	return response, err
}

func DeleteContact(phone *pb.Phone) (*pb.BoolMessageReply, error) {
	response := &pb.BoolMessageReply{}
	err := dbPrepare(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			if contactTmp := bucket.Get([]byte(phone.Number)); contactTmp != nil { // Проверяем наличие номера в БД
				contactFromDB := &pb.Contact{}
				err := proto.Unmarshal(contactTmp, contactFromDB)
				if err != nil {
					return err
				}
				err = bucket.Delete([]byte(phone.Number)) // Удаляем запись с БД
				if err != nil {
					return err
				}
				response.IsOk = true
			} else { // Если номера нет в БД - сообщаем об этом и выходим
				response.IsOk = false
				response.Msg = MsgContactNonExist
			}
		} else { // Если bucket не существует (совсем нет записей) - сообщаем об этом и выходим
			response.IsOk = false
			response.Msg = MsgEmptyAddressBook
		}

		return nil
	}, true) // Открываем Writable транзакцию
	return response, err
}