# goSwaggerGRPC

Клиент-серверное GO-приложение.  Работает по типу телефонной (адресной) книги. Номер телефона является ключом - добавить две
записи с одинаковыми номерами невозможно. Структура Contact является значением для ключа. Проект разбит на два 
приложения: сервер и клиент. Клиент является gRPC-клиентом. Сервер существует в двух версиях: gRPC-сервер и REST-сервер,
запуск второго возможен только когда первый работает.

### Технологии: 
Go, Cobra, Protobuf, gRPC, REST, Swagger, а также база данных BoltDB.

### Загрузка проекта
Использовать любую из команд:<br>
1 - **go get gitlab.com/oleg.kuleba/goSwaggerGRPC** с любой директории<br>
( если не сработает, то использовать комманду go get gitlab.com/oleg.kuleba/goSwaggerGRPC.git и после нее изменить
 название папки проекта с goSwaggerGRPC.git на goSwaggerGRPC )<br>
или<br>
2 - **git clone https://gitlab.com/oleg.kuleba/goSwaggerGRPC.git** в директории %GOPATH%\src\gitlab.com\oleg.kuleba

### Использование
- Сначала запускаем gRPC-сервер **start**
- Приложение готово для работы с gRPC-клиентом (используем gRPC-клиент, смотреть команды клиента/браузера)
- Запускаем REST-сервер (если хотим работать через REST) **startRest**
- Приложение готово для работы с REST-запросами (используем браузер, адрес - http://host:port/rpc/contacts<br>
Детальнее, смотреть команды клиента/браузера).
- Также можно использовать Swagger UI по адресу http://host:port/swaggerui/#/<br> 

**host и port будут указаны в консоли REST-сервера**

Все команды используются в формате go run main.go {command} [arg1, arg2, ..., argN]
Применять команды необходимо, находясь в каталоге приложения (команды сервера в папке goSwaggerGRPC\grpcServer,
команды клиента в папке goSwaggerGRPC\grpcClient).

### Запуск тестов
**(выполняются в то время, когда gRPC-сервер работает)**<br>
в каталоге goSwaggerGRPC\tests выполнить команду **go test** или **go test -v**

### Команды сервера
#### start
Запускает gRPC-сервер<br>
- start<br>
пример: go run main.go start

#### startRest
Запускает REST-сервер. Запуск возможен только когда gRPC-сервер работает<br>
- startRest<br>
пример: go run main.go startRest

#### stop
для остановки сервера необходимо нажать сочетание клавиш Ctrl + C 

### Swagger UI
**(выполняются в то время, когда gRPC-сервер и REST-сервер работают)**<br>
- http://host:port/swaggerui/#/<br>
пример: http://localhost:8282/swaggerui/#/<br>
host и port будут указаны в консоли REST-сервера

### Команды клиента/браузера
**(выполняются в то время, когда gRPC-сервер/REST-сервер работает)**

#### addContact
- addContact номерТелефона имя город улица дом [квартира]<br>
пример: go run main.go addContact +380994445666 Taras Dnipro Titova 26a 45
- POST host/port/rpc/contacts<br>
пример: POST http://localhost:8282/rpc/contacts<br>
в body передаем структуру ContactRequestOrResponse

#### findAll
- findAll<br>
пример: go run main.go findAll
- GET host/port/rpc/contacts<br>
пример: GET http://localhost:8282/rpc/contacts

#### findByNumber
- findByNumber номерТелефона<br>
пример: go run main.go findByNumber +380994445666
- GET host/port/rpc/contacts/{number}<br>
пример: GET http://localhost:8282/rpc/contacts/+380994445666

#### editContact
- editContact номерТелефона имя город улица дом [квартира]<br>
пример: go run main.go editContact +380994445666 Taras Kyiv Shevchenka 15
- PUT host/port/rpc/contacts/{number}<br>
пример: PUT http://localhost:8282/rpc/contacts/+380994445666<br>
в body передаем структуру ContactRequestOrResponse

#### deleteContact
- deleteContact номерТелефона<br>
пример: go run main.go deleteContact +380994445666
- DELETE host/port/rpc/contacts/{number}<br>
пример: DELETE http://localhost:8282/rpc/contacts/+380994445666

### Полезное
В папке goSwaggerGRPC\grpcServer есть файл базы данных phoneBook.db с несколькими записями для удобства тестирования
приложения.<br>
В корне проекта есть файл commands.txt, в котором есть:<br>
- команды для генерирования файлов<br>
- скрипты для работы POST/PUT/DELETE методов в консоли браузера<br>