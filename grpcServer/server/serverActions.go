package server

import (
	"golang.org/x/net/context"
	pb "gitlab.com/oleg.kuleba/goSwaggerGRPC/rpc"
	"net"
	"log"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc"
	"github.com/golang/protobuf/ptypes/empty"
	"errors"
	"flag"
	"net/http"
	"github.com/golang/glog"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	gw "gitlab.com/oleg.kuleba/goSwaggerGRPC/rpc"
	"fmt"
	mux2 "github.com/gorilla/mux"
	"gitlab.com/oleg.kuleba/goSwaggerGRPC/swaggerui"
	"strings"
	"gitlab.com/oleg.kuleba/goSwaggerGRPC/grpcServer/dao"
	"gitlab.com/oleg.kuleba/goSwaggerGRPC/validation"
)

const MsgServerError string = "SERVER ERROR"
const Network string = "tcp"
const Host string = "localhost"
const PortGRPC string = ":50051"
const PortREST string = ":8282"

type server struct{}

func runHttp() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	router := mux2.NewRouter()
	fs := swaggerui.GetFS()
	router.PathPrefix("/swaggerui/").Handler(http.StripPrefix("/swaggerui/", fs)).Name("swaggerui")

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := gw.RegisterAddressBookHandlerFromEndpoint(ctx, mux, fmt.Sprint(Host, PortGRPC), opts)
	if err != nil {
		return err
	}
	//return http.ListenAndServe(PortREST, mux)

	printRoutes(router)
	router.PathPrefix("/rpc/").Handler(mux)
	return http.ListenAndServe(PortREST, router)
}

func StartRestServer() {
	flag.Parse()
	defer glog.Flush()

	if err := runHttp(); err != nil {
		glog.Fatal(err)
	}
}

// print all routes
func printRoutes(r *mux2.Router) {

	err := r.Walk(func(route *mux2.Route, router *mux2.Router, ancestors []*mux2.Route) error {
		t, err := route.GetPathTemplate()
		if err != nil {
			return err
		}
		log.Printf("GetPathTemplate:%v", t)
		qt, err := route.GetQueriesTemplates()
		//if err != nil {
		//	return err
		//}
		log.Printf("GetQueriesTemplates:%v", qt)
		// p will contain regular expression is compatible with regular expression in Perl, Python, and other languages.
		// for instance the regular expression for path '/articles/{id}' will be '^/articles/(?P<v0>[^/]+)$'
		p, err := route.GetPathRegexp()
		if err != nil {
			return err
		}
		log.Printf("GetPathRegexp:%v", p)
		// qr will contain a list of regular expressions with the same semantics as GetPathRegexp,
		// just applied to the Queries pairs instead, e.g., 'Queries("surname", "{surname}") will return
		// {"^surname=(?P<v0>.*)$}. Where each combined query pair will have an entry in the list.
		qr, err := route.GetQueriesRegexp()
		//if err != nil {
		//	return err
		//}
		log.Printf("GetQueriesRegexp:%v", qr)
		m, err := route.GetMethods()
		if err != nil {
			return err
		}
		log.Printf("GetMethods:%v", m)
		log.Println(strings.Join(m, ","), strings.Join(qt, ","), strings.Join(qr, ","), t, p)
		return nil
	})

	if err != nil {
		log.Fatalf("error walking routes:%v", err)
	}

}

func Start() {
	lis, err := net.Listen(Network, PortGRPC)
	if err != nil {
		panic(err)
	}
	serv := grpc.NewServer()
	pb.RegisterAddressBookServer(serv, &server{})
	reflection.Register(serv)
	if err := serv.Serve(lis); err != nil {
		log.Fatal("server.Serve(listener) was failed")
	}
}

func setNegativeResponse(response *pb.BoolMessageReply) *pb.BoolMessageReply {
	response.IsOk = false
	response.Msg = validation.GetValidationMessages()
	return response
}

func (s *server) AddContact(ctx context.Context, in *pb.ContactRequestOrResponse) (*pb.BoolMessageReply, error) {
	response := &pb.BoolMessageReply{}
	// Если аргументы не проходят валидацию (все, кроме квартиры, т.к. она опциональная), выводим инфу об этом и выходим
	if !validation.CheckParamsExceptApartment(in.Phone.Number, in.Contact.Name, in.Contact.Address.City, in.Contact.Address.Street, in.Contact.Address.Building) {
		return setNegativeResponse(response), nil
	}
	if in.Contact.Address.Apartment != "" { // Если квартира указана, то
		if !validation.Validate(in.Contact.Address.Apartment, validation.BuildingOrApartmentFlag) { // валидируем ее. Если не валидно - выходим
			return setNegativeResponse(response), nil
		}
	}

	response, err := dao.AddContact(in)
	if err != nil {
		response.IsOk = false
		response.Msg = MsgServerError
		return response, nil
	}
	return response, nil
}

func (s *server) FindAll(empty *empty.Empty, stream pb.AddressBook_FindAllServer) error {
	err := dao.FindAll(stream)
	return err
}

func (s *server) FindByNumber(ctx context.Context, in *pb.Phone) (*pb.ContactRequestOrResponse, error) {
	contactResponse := &pb.ContactRequestOrResponse{}
	if !validation.Validate(in.Number, validation.PhoneFlag) { // Если номер не проходит валидацию, выводим инфу об этом и выходим
		// TODO М/Б добавить статус
		return contactResponse, errors.New(validation.GetValidationMessages())
	}
	contactResponse, err := dao.FindByNumber(in)
	if err != nil {
		// TODO М/Б поставить статус MsgServerError вместо error
		return contactResponse, errors.New(MsgServerError)
	}
	return contactResponse, nil
}

func (s *server) EditContact(ctx context.Context, in *pb.ContactRequestOrResponse) (*pb.BoolMessageReply, error) {
	response := &pb.BoolMessageReply{}
	// Если аргументы не проходят валидацию (все, кроме квартиры, т.к. она опциональная), выводим инфу об этом и выходим
	if !validation.CheckParamsExceptApartment(in.Phone.Number, in.Contact.Name, in.Contact.Address.City, in.Contact.Address.Street, in.Contact.Address.Building) {
		return setNegativeResponse(response), nil
	}
	if in.Contact.Address.Apartment != "" { // Если квартира указана, то
		if !validation.Validate(in.Contact.Address.Apartment, validation.BuildingOrApartmentFlag) { // валидируем ее. Если не валидно - выходим
			return setNegativeResponse(response), nil
		}
	}

	response, err := dao.EditContact(in)
	if err != nil {
		response.IsOk = false
		response.Msg = MsgServerError
		return response, nil
	}
	return response, nil
}

func (s *server) DeleteContact(ctx context.Context, in *pb.Phone) (*pb.BoolMessageReply, error) {
	response := &pb.BoolMessageReply{}
	if !validation.Validate(in.Number, validation.PhoneFlag) { // Если номер не проходит валидацию, выводим инфу об этом и выходим
		return setNegativeResponse(response), nil
	}
	response, err := dao.DeleteContact(in)
	if err != nil {
		response.IsOk = false
		response.Msg = MsgServerError
		return response, nil
	}
	return response, nil
}

